# python-opencv docker image

## About
- image is always based on latest stable python/Debian 11 (bullseye) image
- automated builds - daily rebuild images
- multi-arch: amd64, arm32v7, arm64

Docker: https://hub.docker.com/repository/docker/michaelbalser/python-opencv

Docker images available for:
- amd64
- arm64v8
- arm32v7

## Multi-arch Image
The below commands reference a
[Docker Manifest List](https://docs.docker.com/engine/reference/commandline/manifest/)
at [`michaelbalser/python-opencv`](https://hub.docker.com/repository/docker/michaelbalser/python-opencv).

Simply running commands using this image will pull
the matching image architecture (e.g. `amd64`, `arm32v7`, or `arm64`) based on
the hosts architecture. Hence, if you are on a **Raspberry Pi** the below
commands will work the same as if you were on a traditional `amd64`
desktop/laptop computer.

## Usage

````
TBD
````
