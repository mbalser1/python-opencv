ARG ARCH=amd64
FROM $ARCH/python:bullseye

MAINTAINER Michael Balser <michael@balser.cc>

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Michael Balser <michael@balser.cc>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="michaelbalser/python-opencv" \
  org.label-schema.description="python-opencv docker image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/repository/docker/michaelbalser/python-opencv" \
  org.label-schema.vcs-url="https://gitlab.com/mbalser1/python-opencv" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN set -ex \
    && apt update \
    && apt install --yes tini python3-opencv \
    && apt clean autoclean \
    && apt autoremove --yes \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/

ENTRYPOINT ["/usr/bin/tini", "--"]
